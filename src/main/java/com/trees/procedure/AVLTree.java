package com.trees.procedure;

import com.trees.models.AVLNode;
import com.trees.interfaces.ITree;

import java.util.*;

public class AVLTree implements ITree {
    private int size;
    private AVLNode root;

    void updateHeight(AVLNode n) {
        n.height = 1 + Math.max(height(n.left), height(n.right));
    }

    int height(AVLNode n) {
        return n == null ? -1 : n.height;
    }

    int getBalance(AVLNode n) {
        return (n == null) ? 0 : height(n.right) - height(n.left);
    }

    AVLNode rotateRight(AVLNode y) {
        AVLNode x = y.left;
        AVLNode z = x.right;
        x.right = y;
        y.left = z;
        updateHeight(y);
        updateHeight(x);
        return x;
    }

    AVLNode rotateLeft(AVLNode y) {
        AVLNode x = y.right;
        AVLNode z = x.left;
        x.left = y;
        y.right = z;
        updateHeight(y);
        updateHeight(x);
        return x;
    }

    AVLNode rebalance(AVLNode z) {
        updateHeight(z);
        int balance = getBalance(z);
        if (balance > 1) {
            if (height(z.right.right) > height(z.right.left)) {
                z = rotateLeft(z);
            } else {
                z.right = rotateRight(z.right);
                z = rotateLeft(z);
            }
        } else if (balance < -1) {
            if (height(z.left.left) > height(z.left.right))
                z = rotateRight(z);
            else {
                z.left = rotateLeft(z.left);
                z = rotateRight(z);
            }
        }
        return z;
    }

    @Override
    public void add(int val)
    {
        try{
            root = add(root, val);
            size++;
        }
        catch (RuntimeException e) {
            //do later
        }
    }

    AVLNode add(AVLNode node, int val) {
        if (node == null) {
            return new AVLNode(val);
        } else if (node.item > val) {
            node.left = add(node.left, val);
        } else if (node.item < val) {
            node.right = add(node.right, val);
        } else {
            throw new RuntimeException("duplicate Key!");
        }
        return rebalance(node);
    }

    @Override
    public boolean del(int val)
    {
        try{
            root = delete(root, val);
            size--;
            return true;
        }
        catch (RuntimeException e) {
            //do later
            return false;
        }
    }

    AVLNode delete(AVLNode node, int val) {
        if (node == null) {
            throw new RuntimeException("no such Key!");
        } else if (node.item > val) {
            node.left = delete(node.left, val);
        } else if (node.item < val) {
            node.right = delete(node.right, val);
        } else {
            if (node.left == null || node.right == null) {
                node = (node.left == null) ? node.right : node.left;
            } else {
                AVLNode mostLeftChild = mostLeftChild(node.right);
                node.item = mostLeftChild.item;
                node.right = delete(node.right, node.item);
            }
        }
        if (node != null) {
            node = rebalance(node);
        }
        return node;
    }

    private AVLNode mostLeftChild(AVLNode node) {
        if (node.left == null) {
            return node;
        }
        else {
            return mostLeftChild(node.left);
        }
    }

    @Override
    public void init(int[] arr) {
        clear();
        for (int i = 0; i < arr.length; i++) {
            add(arr[i]);
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        int[] array = sortArrayBubble(toArray());
        for (int i = 0; i < array.length; i++) {
            stringArray += array[i];
            if (i != array.length - 1) {
                stringArray += ", ";
            }
        }
        stringArray += "]";
        System.out.println(stringArray);
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int[] toArray() {
        int[] array = new int[size];
        if (size == 0) {
            array = new int[0];
            return array;
        }
        ArrayList<AVLNode> nodeArrayList = new ArrayList<>();
        Queue<AVLNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            AVLNode node = queue.remove();
            nodeArrayList.add(node);

            if (node.right != null) {
                queue.add(node.right);
            }

            if (node.left != null) {
                queue.add(node.left);
            }
        }

        Iterator<AVLNode> iterator = nodeArrayList.iterator();

        for (int i = 0; i < array.length; i++) {
            AVLNode current = iterator.next();
            array[i] = current.item;
        }

        return array;
    }


    @Override
    public int getWidth() {
        if (root == null)
            return 0;

        int maxwidth = 0;

        Queue<AVLNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty())
        {
            int count = queue.size();

            maxwidth = Math.max(maxwidth, count);

            while (count > 0)
            {
                AVLNode current = queue.poll();

                if (current.left != null)
                {
                    queue.add(current.left);
                }
                if (current.right != null)
                {
                    queue.add(current.right);
                }

                count--;
            }
        }
        return maxwidth;
    }

    @Override
    public int getHeight() {
        if (root == null) {
            return 0;
        }

        Queue<AVLNode> queue = new ArrayDeque<>();
        queue.add(root);

        AVLNode current = null;
        int height = 0;

        while (!queue.isEmpty())
        {
            int count = queue.size();

            while (count > 0)
            {
                current = queue.poll();

                if (current.left != null) {
                    queue.add(current.left);
                }

                if (current.right != null) {
                    queue.add(current.right);
                }

                count--;
            }

            height++;
        }

        return height;
    }

    @Override
    public int nodes() {
        if (root == null) {
            return 0;
        }

        Queue<AVLNode> queue = new LinkedList<>();

        queue.add(root);
        int nodesNotLeaves = 0;

        while (!queue.isEmpty()) {
            int count = queue.size();

            while (count > 0) {
                AVLNode current = queue.poll();

                if (current.left != null) {
                    queue.add(current.left);
                }

                if (current.right != null) {
                    queue.add(current.right);
                }

                if (current.left != null || current.right != null) {
                    nodesNotLeaves++;
                }

                count--;
            }
        }

        return nodesNotLeaves;
    }

    @Override
    public int leaves() {
        int leaves = size - nodes();
        return leaves;
    }

    @Override
    public void reverse() {
        Queue<AVLNode> queue = new LinkedList<>();

        if(root != null) {
            queue.add(root);
        }

        while(!queue.isEmpty()) {
            AVLNode node = queue.poll();

            if(node.left != null){
                queue.add(node.left);
            }
            if(node.right != null){
                queue.add(node.right);
            }

            if (node.left != null || node.right != null) {
                AVLNode temp = node.left;
                node.left = node.right;
                node.right = temp;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AVLTree avlTree = (AVLTree) o;
        return size == avlTree.size && Objects.equals(root, avlTree.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, root);
    }

    private int[] sortArrayBubble(int[] array){
        boolean notSorted = true;
        while (notSorted) {
            notSorted = false;
            for(int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int x = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = x;
                    notSorted = true;
                }
            }
        }
        return array;
    }
}
