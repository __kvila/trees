package com.trees.recursion;

import com.trees.models.Node;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Field;

import static com.trees.recursionTestData.BSTreeTestData.*;


public class BSTreeTest {
    BSTree cut = new BSTree();

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, 47, addTreeTest1),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 55, addTreeTest2)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(Node reflect, int reflectSize, int value, BSTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.add(value);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] delTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 30, deleteTreeTest1, true),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 150, deleteTreeTest2, true)
        };
    }

    @ParameterizedTest
    @MethodSource("delTestArgs")
    void delTest(Node reflect, int reflectSize, int value, BSTree expected, boolean boolExpected)
            throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        boolean actual = cut.del(value);

        Assertions.assertEquals(expected, cut);
        Assertions.assertEquals(boolExpected, actual);
    }

    static Arguments[] clearTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, originalTreeTest1),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, originalTreeTest1)
        };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(Node reflect, int reflectSize, BSTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.clear();

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] sizeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, 23, 1),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 45, 9)
        };
    }

    @ParameterizedTest
    @MethodSource("sizeTestArgs")
    void sizeTest(Node reflect, int reflectSize, int numAdd, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.add(numAdd);
        int actual = cut.size();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, new int[] {}),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, new int[] {30, 40, 50, 100, 150, 175, 200, 250})
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(Node reflect, int reflectSize, int[] expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] printTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, "[]"),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, "[30, 40, 50, 100, 150, 175, 200, 250]")
        };
    }

    @ParameterizedTest
    @MethodSource("printTestArgs")
    void printTest(Node reflect, int reflectSize, String expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        //скорее всего нужен логгер, хотя пока хз
    }

    static Arguments[] initTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT_REVERSE1), 0, new int[] {}, originalTreeTest1),
                Arguments.arguments(Node.deepCopy(ROOT_REVERSE1), 8, new int[] {100, 50, 150, 30, 200, 40, 175, 250}, originalTreeTest2),
        };
    }

    @ParameterizedTest
    @MethodSource("initTestArgs")
    void initTest(Node reflect, int reflectSize, int[] array, BSTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.init(array);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] nodesTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 5)
        };
    }

    @ParameterizedTest
    @MethodSource("nodesTestArgs")
    void nodesTest(Node reflect, int reflectSize, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.nodes();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] leavesTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 3)
        };
    }

    @ParameterizedTest
    @MethodSource("leavesTestArgs")
    void leavesTest(Node reflect, int reflectSize, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.leaves();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getWidthTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 3)
        };
    }

    @ParameterizedTest
    @MethodSource("getWidthTestArgs")
    void getWidthTest(Node reflect, int reflectSize,  int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.getWidth();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getHeightTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, 0),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, 4)
        };
    }

    @ParameterizedTest
    @MethodSource("getHeightTestArgs")
    void getHeightTest(Node reflect, int reflectSize,  int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.getHeight();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] reverseTestArgs() {
        return new Arguments[] {
                Arguments.arguments(Node.deepCopy(ROOT1), 0, reverseTreeTest1),
                Arguments.arguments(Node.deepCopy(ROOT2), 8, reverseTreeTest2)
        };
    }

    @ParameterizedTest
    @MethodSource("reverseTestArgs")
    void reverseTest(Node reflect, int reflectSize, BSTree expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.reverse();

        Assertions.assertEquals(expected, cut);
    }
}
