package com.trees.recursionTestData;

import com.trees.models.Node;
import com.trees.recursion.BSTree;

import java.lang.reflect.Field;

public class BSTreeTestData {
    public static final BSTree originalTreeTest1 = new BSTree();
    public static final BSTree originalTreeTest2 = new BSTree();
    public static final BSTree addTreeTest1 = new BSTree();
    public static final BSTree addTreeTest2 = new BSTree();
    public static final BSTree deleteTreeTest1 = new BSTree();
    public static final BSTree deleteTreeTest2 = new BSTree();
    public static final BSTree reverseTreeTest1 = new BSTree();
    public static final BSTree reverseTreeTest2 = new BSTree();

    public static final Node ROOT1;
    public static final Node ROOT2;
    public static final Node ROOT_ADD1;
    public static final Node ROOT_ADD2;
    public static final Node ROOT_DELETE1;
    public static final Node ROOT_DELETE2;
    public static final Node ROOT_REVERSE1;
    public static final Node ROOT_REVERSE2;

    static {
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_30 = new Node(30);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_30;
        node1_30.right = node1_40;
        node1_150.right = node1_200;
        node1_200.left = node1_175;
        node1_200.right = node1_250;

        ROOT2 = node1_100;
        ROOT1 = null;

        try {
            Field field1 = originalTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(originalTreeTest1, ROOT1);

            Field field1_1 = originalTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(originalTreeTest1, 0);

            Field field2 = originalTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(originalTreeTest2, ROOT2);

            Field field2_1 = addTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(originalTreeTest2, 8);
        } catch (Exception e) {

        }
    }

    static {
        // add
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_30 = new Node(30);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);
        Node node1_55 = new Node(55);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_30;
        node1_30.right = node1_40;
        node1_150.right = node1_200;
        node1_200.left = node1_175;
        node1_200.right = node1_250;
        node1_50.right = node1_55;

        ROOT_ADD2 = node1_100;

        Node node1_47 = new Node(47);

        ROOT_ADD1 = node1_47;

        try {
            Field field1 = addTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(addTreeTest1, ROOT_ADD1);

            Field field1_1 = addTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(addTreeTest1, 1);

            Field field2 = addTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(addTreeTest2, ROOT_ADD2);

            Field field2_1 = addTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(addTreeTest2, 9);
        } catch (Exception e) {

        }
    }

    static {
        // delete
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_40;
        node1_150.right = node1_200;
        node1_200.left = node1_175;
        node1_200.right = node1_250;

        Node node2_100 = new Node(100);
        Node node2_50 = new Node(50);
        Node node2_30 = new Node(30);
        Node node2_200 = new Node(200);
        Node node2_40 = new Node(40);
        Node node2_175 = new Node(175);
        Node node2_250 = new Node(250);

        node2_100.left = node2_50;
        node2_100.right = node2_200;
        node2_50.left = node2_30;
        node2_30.right = node2_40;
        node2_200.left = node2_175;
        node2_200.right = node2_250;

        ROOT_DELETE1 = node1_100;
        ROOT_DELETE2 = node2_100;

        try {
            Field field1 = deleteTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(deleteTreeTest1, ROOT_DELETE1);

            Field field1_1 = deleteTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(deleteTreeTest1, 7);

            Field field2 = deleteTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(deleteTreeTest2, ROOT_DELETE2);

            Field field2_1 = deleteTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(deleteTreeTest2, 7);
        } catch (Exception e) {

        }
    }

    static {
        // reverse
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_30 = new Node(30);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);

        node1_100.right = node1_50;
        node1_100.left = node1_150;
        node1_50.right = node1_30;
        node1_30.left = node1_40;
        node1_150.left = node1_200;
        node1_200.right = node1_175;
        node1_200.left = node1_250;

        ROOT_REVERSE2 = node1_100;
        ROOT_REVERSE1 = null;

        try {
            Field field1 = reverseTreeTest1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(reverseTreeTest1, ROOT_REVERSE1);

            Field field1_1 = reverseTreeTest1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(reverseTreeTest1, 0);

            Field field2 = reverseTreeTest2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(reverseTreeTest2, ROOT_REVERSE2);

            Field field2_1 = reverseTreeTest2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(reverseTreeTest2, 8);
        } catch (Exception e) {

        }
    }
}
